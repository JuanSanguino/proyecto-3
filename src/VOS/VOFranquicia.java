package VOS;

import model.data_structures.HashSondeoLineal;
import model.data_structures.ListaEncadenada;

/**
 *  
 * @Author: Tomas F. Venegas 
 */

public class VOFranquicia implements Comparable<VOFranquicia>{
	
	/**
	 * Atributo que modela el nombre de la franquicia
	 */
	private String nombre;
	
	private HashSondeoLineal<String, VOTeatro> teatros;
	public VOFranquicia() {
		teatros= new HashSondeoLineal<>(10);
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public int compareTo(VOFranquicia arg0) {
		return nombre.compareTo(arg0.nombre);
	}

	public HashSondeoLineal<String, VOTeatro> getTeatros() {
		return teatros;
	}

	public void setTeatros(HashSondeoLineal<String, VOTeatro> teatros) {
		this.teatros = teatros;
	}
	public void agregarElementos (VOTeatro arg)
	{
		this.teatros.put(arg.getNombre(), arg);
	}
}
