package VOS;

public class VOPelicula {
	
	private int id;
	private String titulo;
	private String[] generos;
	private double[] similitudes;

	public VOPelicula(int id, String titulo, String[] generos, double[] similitudes) {
	
		this.id = id;
		this.titulo = titulo;
		this.generos = generos;
		this.similitudes = similitudes;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String[] getGeneros() {
		return generos;
	}
	public void setGeneros(String[] generos) {
		this.generos = generos;
	}
	
	public double[] getSimilitudes() {
		return similitudes;
	}


	public void setSimilitudes(double[] similitudes) {
		this.similitudes = similitudes;
	}


}
