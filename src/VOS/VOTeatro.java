
package VOS;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOTeatro {
	
	/**
	 * Atributo que modela el nombre del teatro
	 */
	
	private String nombre;
	
	/**
	 * Atributo que modela la ubicacion del teatro 
	 */
	
	private VOUbicacion ubicacion;
	
	/**
	 * Atributo que referencia la franquicia
	 */
	
	private String franquicia;
	
	
	public VOTeatro() {
	// TODO Auto-generated constructor stub
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public VOUbicacion getUbicacion() {
		return ubicacion;
	}


	public void setUbicacion(VOUbicacion ubicacion) {
		this.ubicacion = ubicacion;
	}


	public String getFranquicia() {
		return franquicia;
	}


	public void setFranquicia(String franquicia) {
		this.franquicia = franquicia;
	}
}
