package VOS;

import model.data_structures.HashSondeoLineal;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOUsuario {
		
	/**
	 * Nombre de usuario
	 */
	private String name;
	
	/**
	 * Tabla de hash con los ratings del usuario, donde la llave es la id de la pel�cula y el valor el rating
	 */
	private HashSondeoLineal<Integer, Integer> ratings;
	
	public VOUsuario() {
		// TODO Auto-generated constructor stub
	}	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public HashSondeoLineal<Integer, Integer> getRatings() {
		return ratings;
	}
	
	public void setRatings(HashSondeoLineal<Integer, Integer> ratings) {
		this.ratings = ratings;
	}
}
