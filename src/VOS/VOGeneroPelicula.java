package VOS;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOGeneroPelicula implements Comparable<VOGeneroPelicula> {

	/**
	 * nombre del genero
	 */
	
	private String nombre;
	
	public VOGeneroPelicula() {
		// TODO Auto-generated constructor stub
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public int compareTo(VOGeneroPelicula arg0) {
		// TODO Auto-generated method stub
		return nombre.compareTo(arg0.nombre);
	}
	
}
