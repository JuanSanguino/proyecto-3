package VOS;

import java.sql.Date;
import java.sql.Time;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOPeliculaPlan {
	
	/**
	 * Atributo que apunta a la pelicula que se propone
	 */
	private VOPelicula pelicula;
	
	
	/**
	 * Atributo que apunta al teatro que se propone
	 */
	private VOTeatro teatro;
	
	/**
	 * Atributo que modela las funciones de la pelicula en el teatro que se propone
	 */
	private Time[] funciones;
	
	/**
	 * Atributo que modela el dia de presentacion de la pelicla
	 * (1..5)
	 */
	
	private int dia;
	

	public VOPeliculaPlan(VOPelicula pelicula, VOTeatro teatro, Time[] funciones, int dia) {
		// TODO Auto-generated constructor stub
		this.pelicula = pelicula;
		this.teatro = teatro;
		this.funciones = funciones;
		this.dia = dia;
	}

	public Time[] getFunciones() {
		return funciones;
	}
	
	public void setFunciones(Time[] funciones) {
		this.funciones = funciones;
	}
	
	public VOPelicula getPelicula() {
		return pelicula;
	}

	public void setPelicula(VOPelicula pelicula) {
		this.pelicula = pelicula;
	}

	public VOTeatro getTeatro() {
		return teatro;
	}

	public void setTeatro(VOTeatro teatro) {
		this.teatro = teatro;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}
}
