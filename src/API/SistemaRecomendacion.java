package API;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.InputStreamReader;
import java.io.FileInputStream;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import VOS.*;
import model.data_structures.*;

public class SistemaRecomendacion implements ISistemaRecomendacion {


	private  HashSondeoLineal<String, VOFranquicia> teatrosPorFranquicia;

	private  HashSondeoLineal<String, VOTeatro> teatros;

	private  EdgeWeightedGraph red;

	private HashSondeoLineal<Integer,VOPelicula> peliculas;

	private HashSondeoLineal<String, HashSondeoLineal<Integer, VOPelicula>> peliculasPorGenero;

	private HashSondeoLineal<String, HashSondeoLineal<Integer, VOPeliculaPlan>> carteleraDia1;

	private HashSondeoLineal<String, HashSondeoLineal<Integer, VOPeliculaPlan>> carteleraDia2;

	private HashSondeoLineal<String, HashSondeoLineal<Integer, VOPeliculaPlan>> carteleraDia3;

	private HashSondeoLineal<String, HashSondeoLineal<Integer, VOPeliculaPlan>> carteleraDia4;

	private HashSondeoLineal<String, HashSondeoLineal<Integer, VOPeliculaPlan>> carteleraDia5;

	private HashSondeoLineal<Integer, Bag<VOPeliculaPlan>> peliculasDia1;

	private HashSondeoLineal<Integer, Bag<VOPeliculaPlan>> peliculasDia2;

	private HashSondeoLineal<Integer, Bag<VOPeliculaPlan>> peliculasDia3;

	private HashSondeoLineal<Integer, Bag<VOPeliculaPlan>> peliculasDia4;

	private HashSondeoLineal<Integer, Bag<VOPeliculaPlan>> peliculasDia5;

	private HashSondeoLineal<String, Bag<VOPeliculaPlan>> peliculasDiaPorGenero1;

	private HashSondeoLineal<String, Bag<VOPeliculaPlan>> peliculasDiaPorGenero2;

	private HashSondeoLineal<String, Bag<VOPeliculaPlan>> peliculasDiaPorGenero3;

	private HashSondeoLineal<String, Bag<VOPeliculaPlan>> peliculasDiaPorGenero4;

	private HashSondeoLineal<String, Bag<VOPeliculaPlan>> peliculasDiaPorGenero5;


	private Mst mst;
	
	private BreadthFirstSearch bfs;

	@Override
	public ISistemaRecomendacion crearSR() {
		return new SistemaRecomendacion();

	}
	public SistemaRecomendacion()
	{
		peliculas = new HashSondeoLineal<>(10);
		peliculasPorGenero = new HashSondeoLineal<>(10);
		teatros = new HashSondeoLineal<>(100);
		teatrosPorFranquicia = new HashSondeoLineal<>(20);
		carteleraDia1 = new HashSondeoLineal<>(10);
		carteleraDia2 = new HashSondeoLineal<>(10);
		carteleraDia3 = new HashSondeoLineal<>(10);
		carteleraDia4 = new HashSondeoLineal<>(10);
		carteleraDia5 = new HashSondeoLineal<>(10);
		peliculasDia1 = new HashSondeoLineal<>(10);
		peliculasDia2 = new HashSondeoLineal<>(10);
		peliculasDia3 = new HashSondeoLineal<>(10);
		peliculasDia4 = new HashSondeoLineal<>(10);
		peliculasDia5 = new HashSondeoLineal<>(10);
		peliculasDiaPorGenero1 = new HashSondeoLineal<>(10);
		peliculasDiaPorGenero2 = new HashSondeoLineal<>(10);
		peliculasDiaPorGenero3 = new HashSondeoLineal<>(10);
		peliculasDiaPorGenero4 = new HashSondeoLineal<>(10);
		peliculasDiaPorGenero5 = new HashSondeoLineal<>(10);
		red = new EdgeWeightedGraph(200);
	}

	@Override
	public boolean cargarTeatros(String ruta) throws Exception {
		try
		{
			BufferedReader buff;
			buff = new BufferedReader(new InputStreamReader(new FileInputStream(ruta), "UTF-8"));
			String line = null;
			int x = 1;
			String j = "{\"data\":";

			while((line = buff.readLine()) != null){

				if(x == 1){
					line = line.substring(1, line.length());
					x--;
				}
				j+=line;
			}
			j+="}";
			JsonParser json_parser = new JsonParser();
			JsonObject object = json_parser.parse(j).getAsJsonObject();
			JsonArray array = object.get("data").getAsJsonArray();

			for (int i = 0; i < array.size(); i++) {

				JsonObject actual = (JsonObject)array.get(i).getAsJsonObject();
				VOTeatro nuevoTeatro = new VOTeatro();
				VOUbicacion ubicacionDelTeatro = new VOUbicacion();

				String[] ubicacion = actual.get("UbicacionGeografica(Lat|Long)").getAsString().split("\\|");
				double latitud = Double.parseDouble(ubicacion[0]);
				double longitud = Double.parseDouble(ubicacion[1]);
				ubicacionDelTeatro.setLatitud(latitud);
				ubicacionDelTeatro.setLongitud(longitud);


				String franquicia = actual.get("Franquicia").getAsString();
				String nombre = actual.get("Nombre").getAsString();

				nuevoTeatro.setFranquicia(franquicia);
				nuevoTeatro.setNombre(nombre);
				nuevoTeatro.setUbicacion(ubicacionDelTeatro);

				if(teatrosPorFranquicia.contains(franquicia))
				{
					teatrosPorFranquicia.get(franquicia).agregarElementos(nuevoTeatro);
				}
				else
				{
					VOFranquicia nuevaFranquicia = new VOFranquicia();
					nuevaFranquicia.setNombre(franquicia);
					nuevaFranquicia.agregarElementos(nuevoTeatro);
					teatrosPorFranquicia.put(franquicia, nuevaFranquicia);
				}
				teatros.put(nombre, nuevoTeatro);
			}

			buff.close();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Carga las pel�culas del sistema
	 */
	public void cargarPeliculas()
	{
		JsonParser parser = new JsonParser();
		try
		{
			BufferedReader buff = new BufferedReader(new InputStreamReader(new FileInputStream("./data/movies_filtered.json"),"UTF-8"));

			JsonArray movies = parser.parse(buff).getAsJsonArray();
			for(int i = 0; i < movies.size(); i++)
			{
				JsonObject actual = movies.get(i).getAsJsonObject();

				int id = actual.get("movie_id").getAsInt();
				String titulo = actual.get("title").getAsString();
				String gen = actual.get("genres").getAsString();
				String[] generos = gen.split("\\|");
				double[] similitudes = new double[1];

				VOPelicula pelicula = new VOPelicula(id, titulo, generos, similitudes);
				peliculas.put(id,pelicula);


				HashSondeoLineal<Integer, VOPelicula> temp = new HashSondeoLineal<>(10);
				String genero0 = "";
				String genero1 = "";
				String genero2 = "";
				if(generos[0].equals(""))
				{
					genero0 = "NaN";
					if(peliculasPorGenero.contains(genero0))
					{
						temp = peliculasPorGenero.get(genero0);
						temp.put(id, pelicula);
						peliculasPorGenero.put(genero0, temp);
					}
					else
					{
						temp.put(id, pelicula);
						peliculasPorGenero.put(genero0, temp);
					}
				}
				if(generos.length > 1)
					if(generos[1].equals(""))
					{
						genero1 = "NaN";
						if(peliculasPorGenero.contains(genero1))
						{
							temp = peliculasPorGenero.get(genero1);
							temp.put(id, pelicula);
							peliculasPorGenero.put(genero1, temp);
						}
						else
						{
							temp.put(id, pelicula);
							peliculasPorGenero.put(genero1, temp);
						}
					}
				if(generos.length > 2)
					if(generos[2].equals(""))
					{
						genero2 = "NaN";
						if(peliculasPorGenero.contains(genero2))
						{
							temp = peliculasPorGenero.get(genero2);
							temp.put(id, pelicula);
							peliculasPorGenero.put(genero2, temp);
						}
						else
						{
							temp.put(id, pelicula);
							peliculasPorGenero.put(genero2, temp);
						}
					}

					else
					{
						genero0 = generos[0];
						if(peliculasPorGenero.contains(genero0))
						{
							temp = peliculasPorGenero.get(genero0);
							temp.put(id, pelicula);
							peliculasPorGenero.put(genero0, temp);
						}
						if(generos.length > 1)
						{
							genero1 = generos[1];
							if(peliculasPorGenero.contains(genero0))
							{
								temp = peliculasPorGenero.get(genero0);
								temp.put(id, pelicula);
								peliculasPorGenero.put(genero0, temp);
							}
							else
							{
								temp.put(id, pelicula);
								peliculasPorGenero.put(genero0, temp);
							}						
						}
						if(generos.length > 2)
						{
							genero2 = generos[2];
							if(peliculasPorGenero.contains(genero0))
							{
								temp = peliculasPorGenero.get(genero0);
								temp.put(id, pelicula);
								peliculasPorGenero.put(genero0, temp);
							}
							else
							{
								temp.put(id, pelicula);
								peliculasPorGenero.put(genero0, temp);
							}						
						}
						else
						{
							temp.put(id, pelicula);
							peliculasPorGenero.put(genero0, temp);
						}
					}
			}
			cargarSimilitudes();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public void cargarSimilitudes()
	{
		JsonParser parser = new JsonParser();
		try
		{
			BufferedReader buff = new BufferedReader(new InputStreamReader(new FileInputStream("./data/sims.json"),"UTF-8"));

			JsonArray jsonArray = parser.parse(buff).getAsJsonArray();

			for(int i = 0; i < jsonArray.size(); i++)
			{
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				int id = jsonObject.get("movieId").getAsInt();
				String[] similitudes = jsonObject.get("similitudes").getAsString().split(",");
				double[] simi = new double[similitudes.length];
				for(int j = 0; j < similitudes.length; j++)
				{
					if(similitudes[j].contains("["))
					{
						String temp = similitudes[j];
						int location = temp.indexOf("[");
						String sim = temp.substring(location+1, temp.length());
						if(sim.equals("NaN"))
							simi[j] = 0.0;

						else
						{
							double similitud = Double.parseDouble(sim);
							simi[j] = similitud;							
						}

					}
					else if(similitudes[j].contains("]"))
					{
						String temp = similitudes[j];
						int location = temp.indexOf("]");
						String sim = temp.substring(0, location);
						if(sim.equals("NaN"))
							simi[j] = 0.0;

						else
						{
							double similitud = Double.parseDouble(sim);
							simi[j] = similitud;
						}
					}
					else if(similitudes[j].equals("NaN"))
						simi[j] = 0.0;

					else
					{
						double similitud = Double.parseDouble(similitudes[j]);
						simi[j] = similitud;						
					}
				}

				VOPelicula pelicula = peliculas.get(id);
				pelicula.setSimilitudes(simi);
				peliculas.put(id, pelicula);
				double[] temp = pelicula.getSimilitudes();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public boolean cargarCartelera(String ruta) {
		if(!(teatros.isEmpty() && peliculas.isEmpty()))
		{
			JsonParser parser = new JsonParser();
			try
			{
				BufferedReader buff = new BufferedReader(new InputStreamReader(new FileInputStream(ruta),"UTF-8"));
				JsonArray array = parser.parse(buff).getAsJsonArray();
				for(int i = 0; i < array.size(); i++)
				{
					JsonObject jsonObject = array.get(i).getAsJsonObject();
					String nombreTeatro = jsonObject.get("teatros").getAsString();
					JsonObject infoTeatro = jsonObject.get("teatro").getAsJsonObject();
					JsonArray peliculasTeatro = infoTeatro.get("peliculas").getAsJsonArray();
					HashSondeoLineal<Integer, VOPeliculaPlan> temp = new HashSondeoLineal<>(10);
					VOPelicula peliculaAsociada = new VOPelicula(0, "", null, null);
					Time[] funciones = new Time[1];
					int dia = 0;
					for(int j = 0; j < peliculasTeatro.size(); j++)
					{
						JsonObject infoPelicula = peliculasTeatro.get(j).getAsJsonObject();
						int id = infoPelicula.get("id").getAsInt();

						peliculaAsociada = peliculas.get(id);

						int idPeliculaAsociada = peliculaAsociada.getId();

						JsonArray funcionesArray = infoPelicula.get("funciones").getAsJsonArray();
						funciones = new Time[funcionesArray.size()];
						for(int k = 0; k < funcionesArray.size(); k++)
						{
							JsonObject funcion = funcionesArray.get(k).getAsJsonObject();
							String info = funcion.get("hora").getAsString();
							if(info.contains("p"))
							{
								String hora = info.substring(0, info.indexOf(" "));
								String[] aux = hora.split(":");
								int horaAux = Integer.parseInt(aux[0]);
								if(horaAux != 12)
									horaAux += 12;
								aux[0] = String.valueOf(horaAux);
								String tiempo = "";
								for(int z = 0; z < aux.length; z++)
								{
									tiempo += aux[z]+":";
								}
								tiempo += "00";
								Time time = Time.valueOf(tiempo);
								funciones[k] = time;
							}
							else
							{
								String hora = info.substring(0, info.indexOf(" "));
								hora += ":00";
								Time time = Time.valueOf(hora);
								funciones[k] = time;
							}
						}
						VOTeatro teatroAsociado = teatros.get(nombreTeatro);

						VOPeliculaPlan peliculaPlan = new VOPeliculaPlan(null, null, null, 0);
						Bag<VOPeliculaPlan> planPeliculaList = new Bag<>();
						if(ruta.equals("./data/programacion/dia1.json"))
						{
							dia = 1;
							peliculaPlan = new VOPeliculaPlan(peliculaAsociada, teatroAsociado, funciones, dia);
							Iterable<String> iterable = peliculasPorGenero.keys();
							Iterator<String> iterator = iterable.iterator();
							Bag<VOPeliculaPlan> peliculasPorGeneroList = new Bag<>();
							while(iterator.hasNext())
							{
								String generoActual = iterator.next();
								HashSondeoLineal<Integer, VOPelicula> auxiliar = peliculasPorGenero.get(generoActual);

								if(auxiliar.contains(idPeliculaAsociada))
								{
									if(peliculasDiaPorGenero1.contains(generoActual))
									{
										peliculasPorGeneroList = peliculasDiaPorGenero1.get(generoActual);
										peliculasPorGeneroList.add(peliculaPlan);
										peliculasDiaPorGenero1.put(generoActual, peliculasPorGeneroList);
									}
									else
									{
										peliculasPorGeneroList.add(peliculaPlan);
										peliculasDiaPorGenero1.put(generoActual, peliculasPorGeneroList);
									}
								}
							}
							if(!peliculasDia1.isEmpty() && peliculasDia1.contains(idPeliculaAsociada))
							{
								planPeliculaList = peliculasDia1.get(idPeliculaAsociada);
								planPeliculaList.add(peliculaPlan);
								peliculasDia1.put(idPeliculaAsociada, planPeliculaList);
							}
							else
							{
								planPeliculaList.add(peliculaPlan);
								peliculasDia1.put(idPeliculaAsociada, planPeliculaList);								
							}
						}
						else if(ruta.equals("./data/programacion/dia2.json"))
						{
							dia = 2;
							peliculaPlan = new VOPeliculaPlan(peliculaAsociada, teatroAsociado, funciones, dia);
							Iterable<String> iterable = peliculasPorGenero.keys();
							Iterator<String> iterator = iterable.iterator();
							Bag<VOPeliculaPlan> peliculasPorGeneroList = new Bag<>();
							while(iterator.hasNext())
							{
								String generoActual = iterator.next();
								HashSondeoLineal<Integer, VOPelicula> auxiliar = peliculasPorGenero.get(generoActual);

								if(auxiliar.contains(idPeliculaAsociada))
								{
									if(peliculasDiaPorGenero2.contains(generoActual))
									{
										peliculasPorGeneroList = peliculasDiaPorGenero2.get(generoActual);
										peliculasPorGeneroList.add(peliculaPlan);
										peliculasDiaPorGenero2.put(generoActual, peliculasPorGeneroList);
									}
									else
									{
										peliculasPorGeneroList.add(peliculaPlan);
										peliculasDiaPorGenero2.put(generoActual, peliculasPorGeneroList);
									}
								}
							}
							if(!peliculasDia2.isEmpty() && peliculasDia2.contains(idPeliculaAsociada))
							{
								planPeliculaList = peliculasDia2.get(idPeliculaAsociada);
								planPeliculaList.add(peliculaPlan);
								peliculasDia2.put(idPeliculaAsociada, planPeliculaList);
							}
							else
							{
								planPeliculaList.add(peliculaPlan);
								peliculasDia2.put(idPeliculaAsociada, planPeliculaList);								
							}
						}
						else if(ruta.equals("./data/programacion/dia3.json"))
						{
							dia = 3;
							peliculaPlan = new VOPeliculaPlan(peliculaAsociada, teatroAsociado, funciones, dia);
							Iterable<String> iterable = peliculasPorGenero.keys();
							Iterator<String> iterator = iterable.iterator();
							Bag<VOPeliculaPlan> peliculasPorGeneroList = new Bag<>();
							while(iterator.hasNext())
							{
								String generoActual = iterator.next();
								HashSondeoLineal<Integer, VOPelicula> auxiliar = peliculasPorGenero.get(generoActual);

								if(auxiliar.contains(idPeliculaAsociada))
								{
									if(peliculasDiaPorGenero3.contains(generoActual))
									{
										peliculasPorGeneroList = peliculasDiaPorGenero3.get(generoActual);
										peliculasPorGeneroList.add(peliculaPlan);
										peliculasDiaPorGenero3.put(generoActual, peliculasPorGeneroList);
									}
									else
									{
										peliculasPorGeneroList.add(peliculaPlan);
										peliculasDiaPorGenero3.put(generoActual, peliculasPorGeneroList);
									}
								}
							}
							if(!peliculasDia3.isEmpty() && peliculasDia3.contains(idPeliculaAsociada))
							{
								planPeliculaList = peliculasDia3.get(idPeliculaAsociada);
								planPeliculaList.add(peliculaPlan);
								peliculasDia3.put(idPeliculaAsociada, planPeliculaList);
							}
							else
							{
								planPeliculaList.add(peliculaPlan);
								peliculasDia3.put(idPeliculaAsociada, planPeliculaList);								
							}
						}
						else if(ruta.equals("./data/programacion/dia4.json"))
						{
							dia = 4;
							peliculaPlan = new VOPeliculaPlan(peliculaAsociada, teatroAsociado, funciones, dia);
							Iterable<String> iterable = peliculasPorGenero.keys();
							Iterator<String> iterator = iterable.iterator();
							Bag<VOPeliculaPlan> peliculasPorGeneroList = new Bag<>();
							while(iterator.hasNext())
							{
								String generoActual = iterator.next();
								HashSondeoLineal<Integer, VOPelicula> auxiliar = peliculasPorGenero.get(generoActual);

								if(auxiliar.contains(idPeliculaAsociada))
								{
									if(peliculasDiaPorGenero4.contains(generoActual))
									{
										peliculasPorGeneroList = peliculasDiaPorGenero4.get(generoActual);
										peliculasPorGeneroList.add(peliculaPlan);
										peliculasDiaPorGenero4.put(generoActual, peliculasPorGeneroList);
									}
									else
									{
										peliculasPorGeneroList.add(peliculaPlan);
										peliculasDiaPorGenero4.put(generoActual, peliculasPorGeneroList);
									}
								}
							}
							if(!peliculasDia4.isEmpty() && peliculasDia4.contains(idPeliculaAsociada))
							{
								planPeliculaList = peliculasDia4.get(idPeliculaAsociada);
								planPeliculaList.add(peliculaPlan);
								peliculasDia4.put(idPeliculaAsociada, planPeliculaList);
							}
							else
							{
								planPeliculaList.add(peliculaPlan);
								peliculasDia4.put(idPeliculaAsociada, planPeliculaList);								
							}
						}
						else
						{
							dia = 5;
							peliculaPlan = new VOPeliculaPlan(peliculaAsociada, teatroAsociado, funciones, dia);
							Iterable<String> iterable = peliculasPorGenero.keys();
							Iterator<String> iterator = iterable.iterator();
							Bag<VOPeliculaPlan> peliculasPorGeneroList = new Bag<>();
							while(iterator.hasNext())
							{
								String generoActual = iterator.next();
								HashSondeoLineal<Integer, VOPelicula> auxiliar = peliculasPorGenero.get(generoActual);

								if(auxiliar.contains(idPeliculaAsociada))
								{
									if(peliculasDiaPorGenero5.contains(generoActual))
									{
										peliculasPorGeneroList = peliculasDiaPorGenero5.get(generoActual);
										peliculasPorGeneroList.add(peliculaPlan);
										peliculasDiaPorGenero5.put(generoActual, peliculasPorGeneroList);
									}
									else
									{
										peliculasPorGeneroList.add(peliculaPlan);
										peliculasDiaPorGenero5.put(generoActual, peliculasPorGeneroList);
									}
								}
							}
							if(!(peliculasDia5.isEmpty()) && peliculasDia5.contains(idPeliculaAsociada))
							{
								planPeliculaList = peliculasDia5.get(idPeliculaAsociada);
								planPeliculaList.add(peliculaPlan);
								peliculasDia5.put(idPeliculaAsociada, planPeliculaList);									
							}
							else
							{
								planPeliculaList.add(peliculaPlan);
								peliculasDia5.put(idPeliculaAsociada, planPeliculaList);								
							}
						}

						temp.put(idPeliculaAsociada, peliculaPlan);
					}
					if(dia == 1)
						carteleraDia1.put(nombreTeatro, temp);
					else if(dia == 2)
						carteleraDia2.put(nombreTeatro, temp);
					else if(dia == 3)
						carteleraDia3.put(nombreTeatro, temp);
					else if(dia == 4)
						carteleraDia4.put(nombreTeatro, temp);
					else
						carteleraDia5.put(nombreTeatro, temp);
				}			
				return true;
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return false;
	}
	@Override
	public boolean cargarRed(String ruta) {
		try
		{
			BufferedReader buff;
			buff = new BufferedReader(new InputStreamReader(new FileInputStream(ruta), "UTF-8"));
			String line = null;
			int x = 1;
			String j = "{\"data\":";

			while((line = buff.readLine()) != null){

				if(x == 1){
					line = line.substring(1, line.length());
					x--;
				}
				j+=line;
			}
			j+="}";
			JsonParser json_parser = new JsonParser();
			JsonObject object = json_parser.parse(j).getAsJsonObject();
			JsonArray array = object.get("data").getAsJsonArray();


			for (int i = 0; i < array.size(); i++) {

				JsonObject actual = (JsonObject)array.get(i).getAsJsonObject();
				String teatro1 = actual.get("Teatro 1").getAsString();
				String teatro2 = actual.get("Teatro 2").getAsString();
				double pesoTiempo = actual.get("Tiempo (minutos)").getAsDouble();
				red.agregarCamino(teatro1, teatro2, pesoTiempo);
			}
			buff.close();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}


		return false;
	}

	@Override
	public int sizeMovies() {

		// TODO Auto-generated method stub
		return peliculas.size();

	}

	@Override
	public int sizeTeatros() {
		return teatros.size();
	}

	public VOUsuario cargarRequest(String ruta)
	{
		JsonParser parser = new JsonParser();
		try{
			BufferedReader buff = new BufferedReader(new InputStreamReader(new FileInputStream(ruta),"UTF-8"));
			JsonObject jsonObject = parser.parse(buff).getAsJsonObject();
			JsonObject request = jsonObject.get("request").getAsJsonObject();

			String userName = request.get("user_name").getAsString();
			HashSondeoLineal<Integer, Integer> ratingsHash = new HashSondeoLineal<>(10);
			JsonArray ratings = request.get("ratings").getAsJsonArray();
			for(int i = 0; i < ratings.size(); i++)
			{
				JsonObject ratingsInfo = ratings.get(i).getAsJsonObject();
				int peliculaId = ratingsInfo.get("item_id").getAsInt();
				int rating = ratingsInfo.get("rating").getAsInt();
				ratingsHash.put(peliculaId, rating);
			}
			VOUsuario usuario = new VOUsuario();
			System.out.println(userName);
			usuario.setName(userName);
			usuario.setRatings(ratingsHash);
			return usuario;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPeliculas(VOUsuario usuario, int fecha) {
		// TODO Auto-generated method stub
		if(usuario!=null)
		{
			Bag<VOPeliculaPlan> respuesta = new Bag<>();
			EdgeWeightedGraph ref = red;
			ref.marcarTodo();
			HashSondeoLineal<Integer, Integer> ratings = usuario.getRatings();
			ArrayList<Integer> ids = new ArrayList<>();
			Iterable<Integer> idsPeliculas = ratings.keys();
			for (Integer integer : idsPeliculas) {
				ids.add(integer);
			}
			HashSondeoLineal<String, HashSondeoLineal<Integer, VOPeliculaPlan>> cartelera = asignarCartelera(fecha);
			Iterable<String> teatrosCartelera = cartelera.keys();
			for (String string : teatrosCartelera) {
				HashSondeoLineal<Integer, VOPeliculaPlan> peliculasTeatros = cartelera.get(string);
				for(int i = 0; i < ids.size(); i++)
				{
					Integer key = (Integer)ids.get(i);
					if(peliculasTeatros.contains(key))
					{
						respuesta.add(peliculasTeatros.get(key));
					}
				}
				ref.desmarcarVertex(string);
			}
			mst = new Mst(ref);
			mst.graphMst();
			MinPQ<Edge<Vertex>> minPq = mst.getCaminosCortos();
			System.out.println("Funciones:\n");
			for(VOPeliculaPlan voPeliculaPlan : respuesta)
			{
				System.out.println("Teatro: " + voPeliculaPlan.getTeatro().getNombre());
				System.out.println("Funciones:\n");
				Time[] funciones = voPeliculaPlan.getFunciones();
				for(int i = 0; i < funciones.length; i++)
				{
					String horaInicio = funciones[i].toString();
					System.out.println("Hora inicio: " + horaInicio);
				}
			}
			return respuesta;			
		}
		return null;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorFranquicia(VOFranquicia franquicia,
			int fecha, String franja) {
		// TODO Auto-generated method stub
		if(franquicia != null && fecha != 0 && !franja.equals(""))
		{
			Bag<VOPeliculaPlan> respuesta = new Bag<>();
			HashSondeoLineal<String, VOTeatro> teatrosFranquicia = franquicia.getTeatros();
			HashSondeoLineal<String, HashSondeoLineal<Integer, VOPeliculaPlan>> carteleraElegida = asignarCartelera(fecha);
			EdgeWeightedGraph ref = red;
			ref.marcarTodo();
			Iterable<String> iterarTeatros = teatrosFranquicia.keys();
			for (String string : iterarTeatros) {
				if(carteleraElegida.contains(string))
				{
					HashSondeoLineal<Integer, VOPeliculaPlan> peliculasCartelera = carteleraElegida.get(string);
					peliculasCartelera = peliculasEnFranja(carteleraElegida, franja);
					carteleraElegida.put(string, peliculasCartelera);
					ref.desmarcarVertex(string);
				}
			}
			mst = new Mst(ref);
			mst.graphMst();
			MinPQ<Edge<Vertex>> minPq = mst.getCaminosCortos();
			for (Edge<Vertex> edge : minPq) {
				System.out.println(edge.getOrigen().getNombre());
				System.out.println(edge.getDestino().getNombre());
			}
			return respuesta;
		}
		return null;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGenero(VOGeneroPelicula genero,
			VOUsuario usuario) {
		
		Bag<VOPeliculaPlan> dia = (Bag<VOPeliculaPlan>)PlanPorGeneroYDesplazamiento(genero, 1);
		if(dia.size()< ((Bag<VOPeliculaPlan>)PlanPorGeneroYDesplazamiento(genero, 2)).size())
		dia = ((Bag<VOPeliculaPlan>)PlanPorGeneroYDesplazamiento(genero, 2));
		if(dia.size()< ((Bag<VOPeliculaPlan>)PlanPorGeneroYDesplazamiento(genero, 3)).size())
			dia = ((Bag<VOPeliculaPlan>)PlanPorGeneroYDesplazamiento(genero, 3));
		if(dia.size()< ((Bag<VOPeliculaPlan>)PlanPorGeneroYDesplazamiento(genero, 4)).size())
			dia = ((Bag<VOPeliculaPlan>)PlanPorGeneroYDesplazamiento(genero, 4));
		if(dia.size()< ((Bag<VOPeliculaPlan>)PlanPorGeneroYDesplazamiento(genero, 5)).size())
			dia = ((Bag<VOPeliculaPlan>)PlanPorGeneroYDesplazamiento(genero, 6));
		
		return dia;
	}


	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(
			VOGeneroPelicula genero, int fecha) {
		
		Bag<VOPeliculaPlan> rta = new Bag<>();
		HashSondeoLineal<String, Bag<VOPeliculaPlan>> genereosCartelera = asignarGeneroCartelera(fecha);
		HashSondeoLineal<String, HashSondeoLineal<Integer, VOPeliculaPlan>> generosDia = asignarCartelera(fecha);
		EdgeWeightedGraph ref = red;
		ref.marcarTodo();
		Bag<VOPeliculaPlan> bag = genereosCartelera.get(genero.getNombre());
		for (VOPeliculaPlan voPeliculaPlan : bag)	
			if(voPeliculaPlan.getTeatro()!=null)
			{
				if(ref.contains(voPeliculaPlan.getTeatro().getNombre()))
					ref.desmarcarVertex(voPeliculaPlan.getTeatro().getNombre());
			}
		mst = new Mst(ref);
		mst.graphMst();		
		MinPQ<Edge<Vertex>> caminos = mst.getCaminosCortos();
		Bag<String> teatros = new Bag<>();
		for (Edge<Vertex> edge : caminos) {
			if(!teatros.contains(edge.getDestino().getNombre()))
				teatros.add(edge.getDestino().getNombre());
			if(!teatros.contains(edge.getOrigen().getNombre()))
				teatros.add(edge.getOrigen().getNombre());
		}
		for (String string : teatros) {
			if(string !=null){
				if(generosDia.contains(string))
				{
					ArrayList<VOPeliculaPlan> ref2 = generosDia.get(string).values();
					for (VOPeliculaPlan voPeliculaPlan : ref2) {
						if(bag.contains(voPeliculaPlan))
						{
							rta.add(voPeliculaPlan);
						}
					}
				}
			}
		}
		return rta;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(
			VOGeneroPelicula genero, int fecha, VOFranquicia franquicia) {
		// TODO Auto-generated method stub
		Bag<VOPeliculaPlan> rta = new Bag<>();
		HashSondeoLineal<String, Bag<VOPeliculaPlan>> genereosCartelera = asignarGeneroCartelera(fecha);
		EdgeWeightedGraph ref = red;
		ref.marcarTodo();
		Bag<VOPeliculaPlan> bag = genereosCartelera.get(genero.getNombre());
		for (VOPeliculaPlan voPeliculaPlan : bag)	
			if(voPeliculaPlan.getTeatro()!=null)
				if(ref.contains(voPeliculaPlan.getTeatro().getNombre()))
					ref.desmarcarVertex(voPeliculaPlan.getTeatro().getNombre());
		
		mst = new Mst(ref);
		mst.graphMst();		
		MinPQ<Edge<Vertex>> caminos = mst.getCaminosCortos();
		for (Edge<Vertex> edge : caminos) {
			System.out.println(edge.getDestino().getNombre());
		}
		return null;
	}

	@Override
	public ILista<IEdge<VOTeatro>> generarMapa() throws Exception {
		mst = new Mst(red);
		EdgeWeightedGraph rta = mst.graphMst();
		if(rta.getVertices().size()==red.getVertices().size())
		{
			MinPQ<EdgeTeatros<VOTeatro>> caminos = new MinPQ<>();
			ArrayList<Vertex> vertices = rta.getVertices().values();
			for (Vertex vertex : vertices) {
				Bag<Edge<Vertex>> bag= vertex.getAdj();
				for (Edge<Vertex> edge : bag) {
					VOTeatro destino = teatros.get(edge.getDestino().getNombre());
					VOTeatro origen = teatros.get(edge.getOrigen().getNombre());
					EdgeTeatros<VOTeatro>  nuevo = new EdgeTeatros<VOTeatro>(destino, origen, edge.getPeso());
					caminos.insert(nuevo);
				}
			}
			return caminos;
		}
		else
		{
			throw new Exception("no todos los vertices estan conectados");
		}

	}

	@Override
	public ILista<ILista<VOTeatro>> rutasPosible(VOTeatro origen, int n) {
		// TODO Auto-generated method stub
		Bag<ILista<VOTeatro>> respuesta = new Bag<>();
		EdgeWeightedGraph ref = red;
		bfs = new BreadthFirstSearch(ref, origen.getNombre(), n);
		return respuesta;
	}
	public HashSondeoLineal<String, HashSondeoLineal<Integer, VOPeliculaPlan>>  asignarCartelera(int fecha)
	{
		if(fecha==1) return carteleraDia1;
		else if(fecha==2) return carteleraDia2;
		else if(fecha==3) return carteleraDia3;
		else if(fecha==4) return carteleraDia4;
		else return carteleraDia5;
	}
	public HashSondeoLineal<String, Bag<VOPeliculaPlan>> asignarGeneroCartelera(int fecha)
	{
		if(fecha==1) return peliculasDiaPorGenero1;
		else if(fecha==2) return peliculasDiaPorGenero2;
		else if(fecha==3) return peliculasDiaPorGenero3;
		else if(fecha==4) return peliculasDiaPorGenero4;
		else return peliculasDiaPorGenero5;
	}
	
	public HashSondeoLineal<Integer,VOPeliculaPlan> peliculasEnFranja(HashSondeoLineal<String, HashSondeoLineal<Integer, VOPeliculaPlan>> cartelera, String franja)
	{
		HashSondeoLineal<Integer,VOPeliculaPlan> respuesta = new HashSondeoLineal<>(10);
		Iterable<String> teatros = cartelera.keys();
		for (String string : teatros) {
			ArrayList<VOPeliculaPlan> peliculas = cartelera.get(string).values();
			for (VOPeliculaPlan voPeliculaPlan : peliculas) {
				Time[] funcionesPelicula = voPeliculaPlan.getFunciones();
				int idPelicula = voPeliculaPlan.getPelicula().getId();
				for(int i = 0; i < funcionesPelicula.length; i++)
				{
					String time = funcionesPelicula[i].toString();
					String[] tiempos = time.split(":");
					int hora = Integer.parseInt(tiempos[0]);
					if(franja.compareToIgnoreCase("ma�ana") == 0)
					{
						if(hora < 12)
						respuesta.put(idPelicula,voPeliculaPlan);
					}
					if(franja.compareToIgnoreCase("tarde") == 0)
					{
						if(hora < 18)
						respuesta.put(idPelicula,voPeliculaPlan);
					}
					if(franja.compareToIgnoreCase("noche") == 0)
					{
						if(hora >= 18 )
						respuesta.put(idPelicula,voPeliculaPlan);
					}					
				}
			}
		}
		return respuesta;
	}
	public static void main(String[] args) {
		SistemaRecomendacion mundo = new SistemaRecomendacion();
		try {
			mundo.cargarPeliculas();
			mundo.cargarTeatros("./data/teatros_v4.json");
			mundo.cargarCartelera("./data/programacion/dia1.json");
			mundo.cargarRed("./data/tiemposIguales_v2.json");
			VOGeneroPelicula genero = new VOGeneroPelicula();
			genero.setNombre("Action");
			mundo.PlanPorGeneroYDesplazamiento(genero, 1);
			mundo.cargarRed("./data/tiemposIguales_v2.json");
			mundo.generarMapa();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
