package model.data_structures;

import java.util.ArrayList;

public class BreadthFirstSearch{
	
	private String s;
	
	public BreadthFirstSearch(EdgeWeightedGraph graph, String s, int n)
	{
		HashSondeoLineal<String, Vertex> vertices = graph.getVertices();
		int contador = 0;
		Iterable<String> cantidad = vertices.keys();
		for (String string : cantidad) {
			contador++;
		}
		this.s = s;
		bfs(graph, s, n);
	}
	
	private void bfs(EdgeWeightedGraph graph, String s, int n)
	{
		Queue<String> queue = new Queue<>();
		HashSondeoLineal<String, Vertex> vertices = graph.getVertices();
		Vertex s1 = vertices.get(s);
		s1.setMarcado(true);
		queue.enqueue(s);
		while(!queue.isEmpty())
		{
			String v = queue.dequeue();
			Vertex w = vertices.get(v);
			if(!w.isMarcado())
			{
				w.setMarcado(true);
				queue.enqueue(v);
				if(queue.size() == n)
					break;
			}	
		}
	}

}
