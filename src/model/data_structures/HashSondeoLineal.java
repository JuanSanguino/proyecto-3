package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

import API.ILista;

public class HashSondeoLineal<K extends Comparable<K>, V> implements ILista<V> 
{
	private int n;
	private int m;
	private K[] keys;
	private V[] valores;
	
	
	public HashSondeoLineal(int capacidad)
	{
		if(capacidad != 0)
			m = capacidad;
		else
			m = 1;
		n = 0;
		keys = (K[]) new Comparable[m];
		valores = (V[]) new Object[m];	
	}
	
	public int size()
	{
		return n;
	}
	
	public boolean isEmpty()
	{
		return size() == 0;
	}
	
	public boolean contains(K key)
	{
		return get(key) != null;
	}
	
	private int hash(K key)
	{
		return ((key.hashCode() & 0x7fffffff) % m);
	}
	
	private void resize(int capacidad)
	{
		HashSondeoLineal<K, V> temp = new HashSondeoLineal<>(capacidad);
		for(int i = 0; i < m; i++)
		{
			if(keys[i] != null)
			{
				temp.put(keys[i], valores[i]);
			}
		}
		
		keys = temp.keys;
		valores = temp.valores;
		m = temp.m;
	}
	
	public void put(K key, V val)
	{
		if(val == null)
		{
			delete(key);
			return;
		}
		
		if(n >= m/2)
			resize(2*m);
		
		int i;
		for(i = hash(key); keys[i] != null; i = (i+1) % m)
		{
			if(keys[i].equals(key))
			{
				valores[i] = val;
				return;
			}
		}
		keys[i] = key;
		valores[i] = val;
		n++;
	}
	
	public V get(K key)
	{
		for(int i = hash(key); keys[i] != null; i = ((i+1) % m))
		{
			if(keys[i].equals(key))
				return valores[i];
		}
		return null;
	}
	
	public void delete(K key)
	{
		if(!contains(key))
			return;
		
		int i = hash(key);
		while(!key.equals(keys[i]))
		{
			i = (i+1) % m;
		}
		
		keys[i] = null;
		valores[i] = null;
		
		i = (i+1) % m;
		while(keys[i] != null)
		{
			K keyToRehash = keys[i];
			V valueToRehash = valores[i];
			keys[i] = null;
			valores[i] = null;
			n--;
			put(keyToRehash, valueToRehash);
			i = (i + 1) % m;
		}
		
		n--;
		
		if(n > 0 && n <= m/8)
			resize(m/2);
	}
	
	public Iterable<K> keys()
	{
		Queue<K> queue = new Queue<>();
		for(int i = 0; i < m; i++)
			if(keys[i] != null)
				queue.enqueue(keys[i]);
		
		return queue;
	}
	
	public ArrayList<V> values()
	{
		ArrayList<V> respuesta = new ArrayList<>() ;
		Iterable<K> iter = keys();
		Iterator<K> iterator = iter.iterator();
		for(int i = 0; i < n && iterator.hasNext(); i++)
		{
			K actual = iterator.next();
			respuesta.add(get(actual));
		}
		return respuesta;
	}
}
