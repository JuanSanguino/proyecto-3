package model.data_structures;

import API.ILista;

public class Edge<Vertex> implements Comparable<Edge<Vertex>> {

	private Vertex destino;
	
	private Vertex origen;
	
	private double peso;
	
	private boolean marcado;
	
	public Edge(Vertex destino, Vertex origen, double peso) {
		super();
		this.destino = destino;
		this.origen = origen;
		this.peso = peso;
		this.marcado = false;
	}

	
	public boolean isMarcado() {
		return marcado;
	}


	public void setMarcado(boolean marcado) {
		this.marcado = marcado;
	}


	public Vertex getDestino() {
		return destino;
	}

	public void setDestino(Vertex destino) {
		this.destino = destino;
	}

	public Vertex getOrigen() {
		return origen;
	}

	public void setOrigen(Vertex origen) {
		this.origen = origen;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	@Override
	public int compareTo(Edge<Vertex> arg) {
		if(this.peso> arg.peso)
		{
			return 1;
		}
		else if(this.peso < arg.peso)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
	
	
}
