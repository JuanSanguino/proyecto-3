package model.data_structures;

import java.util.ArrayList;
import java.util.Comparator;

public class Mst {

	private MinPQ<Edge<Vertex>>  caminos;
		
	private double peso;
	
	private EdgeWeightedGraph mst;
	
	private MinPQ<Edge<Vertex>>  caminosCortos;
	
	private boolean tenerEnCuentaLaHora;

	public Mst (EdgeWeightedGraph graph)
	{
		caminosCortos = new MinPQ<Edge<Vertex>>();
		tenerEnCuentaLaHora = false;
		caminos = new MinPQ<Edge<Vertex>>() ;
		mst = new EdgeWeightedGraph(10);
		peso = 0.0;
		ArrayList<Vertex> vertices = graph.getVertices().values();
		for (Vertex vertex : vertices) {
			Bag<Edge<Vertex>> bag= vertex.getAdj();
			for (Edge<Vertex> edge : bag) {
				caminos.insert(edge);
			}
		}
	}
	public EdgeWeightedGraph graphMst()
	{		
		for (Edge<Vertex> edge : caminos) {
	
		
		if(!mst.contains(edge.getDestino().getNombre()) && !mst.contains(edge.getOrigen().getNombre()) )
			{
				mst.agregarCamino(edge.getOrigen().getNombre(), edge.getDestino().getNombre(), edge.getPeso());
				mst.marcarVertex(edge.getDestino().getNombre());
				mst.marcarVertex(edge.getOrigen().getNombre());
				peso += edge.getPeso();
				caminosCortos.insert(edge);
			}
			else if(!mst.contains(edge.getDestino().getNombre()) || !mst.contains(edge.getOrigen().getNombre()) )
			{
				mst.agregarCamino(edge.getDestino().getNombre(), edge.getOrigen().getNombre(), edge.getPeso());
				mst.marcarVertex(edge.getDestino().getNombre());
				mst.marcarVertex(edge.getOrigen().getNombre());
				peso += edge.getPeso();
				caminosCortos.insert(edge);
				
			}
			else if(!mst.getVertex(edge.getDestino().getNombre()).isMarcado())
			{
				mst.agregarCamino(edge.getDestino().getNombre(), edge.getOrigen().getNombre(), edge.getPeso());
				mst.marcarVertex(edge.getDestino().getNombre());
				mst.marcarVertex(edge.getOrigen().getNombre());
				peso += edge.getPeso();
				caminosCortos.insert(edge);
			}
		}
		mst.setPeso(peso);
		mst.desmarcarTodos();
		return mst;
	}
	public void setTenerEnCuentaLaHora(boolean tenerEnCuentaLaHora) {
		this.tenerEnCuentaLaHora = tenerEnCuentaLaHora;
	}
	public MinPQ<Edge<Vertex>> getCaminosCortos() {
		return caminosCortos;
	}
	
	
}
