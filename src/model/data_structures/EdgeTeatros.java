package model.data_structures;

import API.IEdge;
import API.ILista;

public class EdgeTeatros<K> implements Comparable<EdgeTeatros<K>>, IEdge<K> {

	private K destino;
	
	private K origen;
	
	private double peso;
	
	private boolean marcado;
	
	public EdgeTeatros(K destino, K origen, double peso) {
		super();
		this.destino = destino;
		this.origen = origen;
		this.peso = peso;
		this.marcado = false;
	}

	
	public boolean isMarcado() {
		return marcado;
	}


	public void setMarcado(boolean marcado) {
		this.marcado = marcado;
	}


	public K getDestino() {
		return destino;
	}

	public void setDestino(K destino) {
		this.destino = destino;
	}

	public K getOrigen() {
		return origen;
	}

	public void setOrigen(K origen) {
		this.origen = origen;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}


	@Override
	public int compareTo(EdgeTeatros<K> arg) {
		if(this.peso> arg.peso)
		{
			return 1;
		}
		else if(this.peso < arg.peso)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}

}
