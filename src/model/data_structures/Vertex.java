package model.data_structures;

public class Vertex implements Comparable<Vertex> {

	private String nombre;
	
	private Bag<Edge<Vertex>> adj;
	
	private boolean marcado;
	
	public Vertex() {

		adj = new Bag<Edge<Vertex>>();
		marcado = false;
	}
	
	public boolean isMarcado() {
		return marcado;
	}



	public void setMarcado(boolean marcado) {
		this.marcado = marcado;
	}



	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Bag<Edge<Vertex>> getAdj() {
		return adj;
	}

	public void setAdj(Bag<Edge<Vertex>> adj) {
		this.adj = adj;
	}
	
	public void agregarEdge(Vertex destino, double peso )
	{
		Edge nuevo = new Edge<Vertex>(destino, this, peso);
		adj.add(nuevo);
	}

	@Override
	public int compareTo(Vertex arg0) {
		return this.nombre.compareTo(arg0.nombre);
	}
	
	
}
