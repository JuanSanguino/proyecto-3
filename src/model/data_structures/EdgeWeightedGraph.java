package model.data_structures;

import java.util.ArrayList;

public class EdgeWeightedGraph {

	private HashSondeoLineal<String, Vertex> vertices;
	
	private double peso;
	
	private int numeroArcos;
	
	public EdgeWeightedGraph (int v)
	{
		numeroArcos =0;
		vertices = new HashSondeoLineal<>(v);
		peso =0.0;
	}

	public HashSondeoLineal<String, Vertex> getVertices() {
		return vertices;
	}

	public void setVertices(HashSondeoLineal<String, Vertex> vertices) {
		this.vertices = vertices;
	}
	
	public void agregarCamino (String nombreInicial, String nombreFinal, double peso){
		
		if(vertices.contains(nombreInicial) && vertices.contains(nombreFinal))
		{
			vertices.get(nombreInicial).agregarEdge(vertices.get(nombreFinal), peso);
			vertices.get(nombreFinal).agregarEdge(vertices.get(nombreInicial), peso);
			numeroArcos+=2;
		}
		else if(vertices.contains(nombreInicial) && !vertices.contains(nombreFinal))
		{
			Vertex nuevo = new Vertex();
			nuevo.setNombre(nombreFinal);
			vertices.put(nombreFinal, nuevo);
			vertices.get(nombreInicial).agregarEdge(nuevo, peso);
			vertices.get(nombreFinal).agregarEdge(vertices.get(nombreInicial), peso);
			numeroArcos+=2;
		}
		else if(!vertices.contains(nombreInicial) && vertices.contains(nombreFinal))
		{
			Vertex nuevo = new Vertex();
			nuevo.setNombre(nombreInicial);
			vertices.put(nombreInicial, nuevo);
			vertices.get(nombreFinal).agregarEdge(nuevo, peso);
			vertices.get(nombreInicial).agregarEdge(vertices.get(nombreFinal), peso);
			numeroArcos+=2;
		}
		else if (!vertices.contains(nombreInicial) && !vertices.contains(nombreFinal))
		{
			Vertex nuevo1 = new Vertex();
			nuevo1.setNombre(nombreFinal);
			Vertex nuevo2 = new Vertex();
			nuevo2.setNombre(nombreInicial);
			vertices.put(nombreFinal, nuevo1);
			vertices.put(nombreInicial, nuevo2);
			vertices.get(nombreInicial).agregarEdge(nuevo1, peso);
			vertices.get(nombreFinal).agregarEdge(nuevo2, peso);
			numeroArcos+=2;
		}
	}
		public Vertex getVertex(String vertex)
		{
			return vertices.get(vertex);
		}
		
		public void marcarVertex (String nombre)
		{
			vertices.get(nombre).setMarcado(true);
		}
		public void desmarcarVertex(String nombre)
		{
			vertices.get(nombre).setMarcado(false);
		}
		
		public boolean contains (String nombre)
		{
			return vertices.contains(nombre);
		}

		public double getPeso() {
			return peso;
		}

		public void setPeso(double peso) {
			this.peso = peso;
		}

		public int getNumeroArcos() {
			return numeroArcos;
		}
		public void desmarcarTodos()
		{
			ArrayList<Vertex> adj =vertices.values();
			for (Vertex vertex : adj) {
				vertex.setMarcado(false);
			}
		}
		public void marcarTodo()
		{
			ArrayList<Vertex> adj =vertices.values();
			for (Vertex vertex : adj) {
				vertex.setMarcado(true);
			}
		}
	}
