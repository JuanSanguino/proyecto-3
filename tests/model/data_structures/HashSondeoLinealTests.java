package model.data_structures;

import junit.framework.*;
 
public class HashSondeoLinealTests extends TestCase 
{
	private HashSondeoLineal<Integer, String> sondeoLineal;
	
	private void setupEscenario0()
	{
		sondeoLineal = new HashSondeoLineal<>(0);
	}
	private void setupEscenario1()
	{
		sondeoLineal = new HashSondeoLineal<>(1);
		sondeoLineal.put(0, "a");
	}
	private void setupEscenario2()
	{
		sondeoLineal = new HashSondeoLineal<>(5);
		sondeoLineal.put(0, "a");
		sondeoLineal.put(1, "b");
		sondeoLineal.put(2, "c");
		sondeoLineal.put(3, "d");
		sondeoLineal.put(4, "e");
	}
	
	public void testSize()
	{
		setupEscenario0();
		assertEquals("No deber�a tener ning�n elemento", 0, sondeoLineal.size());
		
		setupEscenario1();
		assertEquals("Deber�a tener un elemento", 1, sondeoLineal.size());
		
		setupEscenario2();
		assertEquals("Deber�a tener m�s de un elemento", 5, sondeoLineal.size());
	}
	
	public void testIsEmpty()
	{
		setupEscenario0();
		assertTrue("Deber�a estar vac�a", sondeoLineal.isEmpty());
		
		setupEscenario1();
		assertFalse("No deber�a estar vac�a", sondeoLineal.isEmpty());
		
		setupEscenario2();
		assertFalse("No deber�a estar vac�a", sondeoLineal.isEmpty());
	}
	
	public void testContains()
	{
		setupEscenario0();
		assertFalse("No deber�a contener ning�n elemento", sondeoLineal.contains(0));
		
		setupEscenario1();
		assertTrue("Deber�a contener el elemento", sondeoLineal.contains(0));
		
		setupEscenario2();
		assertTrue("Deber�a contener el elemento", sondeoLineal.contains(3));
	}
	
	public void testGet()
	{
		setupEscenario0();
		assertNull("No deber�a recuperar ning�n elemento", sondeoLineal.get(1));
		
		setupEscenario1();
		assertEquals("Deber�a retornar el elemento dado", "a", sondeoLineal.get(0));
		
		setupEscenario2();
		assertEquals("Deber�a retornar el elemento dado", "c", sondeoLineal.get(2));
	}
	
	public void testDelete()
	{
		setupEscenario0();
		sondeoLineal.delete(0);
		assertNull("No deber�a borrar ning�n elemento", sondeoLineal.get(0));
		
		setupEscenario1();
		sondeoLineal.delete(0);
		assertNull("Deber�a borrar el �nico elemento en la tabla", sondeoLineal.get(0));
		
		setupEscenario2();
		sondeoLineal.delete(2);
		assertNull("Deber�a haber borrado el elemento especificado", sondeoLineal.get(2));
	}
}
