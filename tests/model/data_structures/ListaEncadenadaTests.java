package model.data_structures;

import junit.framework.TestCase;

public class ListaEncadenadaTests extends TestCase {

	private ListaEncadenada<Integer,String> listPrueba;
	
	private Bag<String> bagPrueba;
	
	private void setupEscenario0()
	{
		listPrueba = new ListaEncadenada<Integer,String>();
		bagPrueba = new Bag<>();
	}
	private void setupEscenario1()
	{
		listPrueba = new ListaEncadenada<Integer,String>();
		listPrueba.agregarElementoFinal(1,"a");
		listPrueba.agregarElementoFinal(2,"b");
		
		bagPrueba.add("a");
		bagPrueba.add("b");
		bagPrueba.add("c");
	}
	
	public void testdarElemento()
	{	
		setupEscenario0();
		assertNull("No deber�a retornar ning�n objeto", listPrueba.darElemento(0));
		
		setupEscenario1();
		assertEquals("No se da el elemento entregado por posici�n", "b", listPrueba.darElemento(1));
	}
	public void testdarNumeroElementos()
	{	
		setupEscenario0();
		assertEquals("No deber�a haber ning�n elemento", 0, listPrueba.darNumeroElementos());
		
		setupEscenario1();
		assertEquals("Deber�a haber al menos un elemento en la lista", 2, listPrueba.darNumeroElementos());
	}
	
	public void testdarElementoPosicionActual()
	{	
		setupEscenario0();
		assertEquals("No deber�a existir ning�n elemento", null, listPrueba.darElementoPosicionActual());
		
		setupEscenario1();
		assertEquals("No da el elemento de la posici�n actual", "a", listPrueba.darElementoPosicionActual());
	}
	
	public void testavanzarSiguienteTest()
	{	
		setupEscenario0();
		assertFalse("No deber�a poder avanzar la posici�n", listPrueba.avanzarSiguientePosicion());
		
		setupEscenario1();
		assertTrue("Deber�a avanzar a la siguiente posici�n", listPrueba.avanzarSiguientePosicion());
	}
	
	public void testeliminarObjeto()
	{		
		setupEscenario0();
		assertEquals("No deber�a eliminar ning�n objeto", null, listPrueba.eliminarObjeto(0));
		
		setupEscenario1();
		assertEquals("Deber�a eliminar el elemento de la posici�n dada", "b", listPrueba.eliminarObjeto(1));
	}
	
	public void testcontains()
	{
		setupEscenario0();
		assertFalse("No deber�a contenerlo", bagPrueba.contains("a"));
		
		setupEscenario1();
		assertTrue("Deber�a contener el elemento", bagPrueba.contains("a"));
		
		assertTrue("Deber�a contener el elemento", bagPrueba.contains("b"));
		
		assertFalse("No deber�a contener el elemento", bagPrueba.contains("f"));
	}
}
