package model.data_structures;

import junit.framework.TestCase;

public class QueueTests extends TestCase {

	private Queue<String> cola;
	
	private void setupEscenario0()
	{
		cola = new Queue<>();
	}
	
	private void setupEscenario1()
	{
		cola = new Queue<>();
		cola.enqueue("a");
	}
	
	private void setupEscenario2()
	{
		cola = new Queue<>();
		cola.enqueue("a");
		cola.enqueue("b");
		cola.enqueue("c");
	}
	
	public void testIsEmpty()
	{
		setupEscenario0();
		assertTrue("La cola deber�a estar vac�a", cola.isEmpty());
		
		setupEscenario1();
		assertFalse("La cola deber�a tener al menos un elemento", cola.isEmpty());
		
		setupEscenario2();
		assertFalse("La cola tiene m�s de un elemento", cola.isEmpty());
	}
	
	public void testSize()
	{
		setupEscenario0();
		assertEquals("La cola no deber�a tener elementos", 0, cola.size());
		
		setupEscenario1();
		assertEquals("La cola deber�a tener un elemento", 1, cola.size());
		
		setupEscenario2();
		assertEquals("La cola tiene m�s de un elemento", 3, cola.size());
	}
	
	public void testPeek()
	{
		setupEscenario0();
		assertNull("No deber�a retornar ningun elemento", cola.peek());
		
		setupEscenario1();
		assertEquals("El elemento retornado no corresponde", "a", cola.peek());
	}
	
	public void testDequeue()
	{
		setupEscenario0();
		assertNull("No deber�a retornar ning�n elemento", cola.dequeue());
		
		setupEscenario1();
		assertEquals("El elemento retornado no coinside", "a", cola.dequeue());
		
		setupEscenario2();
		assertEquals("El elemento retornado no coinside", "a", cola.dequeue());
	}
}
