package model.data_structures;

import junit.framework.TestCase;

public class EdgeWeightedGraphTests extends TestCase{

	private EdgeWeightedGraph grafo;
	
	private Mst mst;
	
	public void setupEscenario ()
	{
		grafo = new EdgeWeightedGraph(13);
		grafo.agregarCamino("a", "b", 1);
		grafo.agregarCamino("a", "c", 2);
		grafo.agregarCamino("b", "d", 1);
		grafo.agregarCamino("b", "e", 0.5);
		grafo.agregarCamino("c", "f", 3);
		grafo.agregarCamino("e", "f", 2.5);
		grafo.agregarCamino("e", "c", 0.5);
		
	}
	
	public void setupEscenario2 ()
	{
		grafo = new EdgeWeightedGraph(13);
		grafo.agregarCamino("a", "b", 1);
		grafo.agregarCamino("a", "c", 2);
		grafo.agregarCamino("b", "d", 1);
		grafo.agregarCamino("b", "e", 0.5);
		grafo.agregarCamino("c", "f", 3);
		grafo.agregarCamino("e", "f", 2.5);
		grafo.agregarCamino("e", "c", 0.5);
		grafo.agregarCamino("g", "d", 1);
		
	}
	public void setupEscenario1()
	{
		grafo = new EdgeWeightedGraph(5);
	}
	
	public void setupEscenarioMst()
	{
		setupEscenario();
		mst = new Mst(grafo);
	}
	
	public void setupEscenarioMst1()
	{
		setupEscenario2();
		mst = new Mst(grafo);
	}
	
	public void testGetVertices()
	{
		setupEscenario();
		assertEquals("deberia tener 6 vertices", 6, grafo.getVertices().size());
		
		setupEscenario1();
		assertEquals("deberia estar vacio", 0, grafo.getVertices().size());
	}
	
	public void testMarcarVertex()
	{
		setupEscenario();
		grafo.marcarVertex("a");
		assertTrue("deberia estar marcado", grafo.getVertex("a").isMarcado());
	}
	public void testContains ()
	{
		setupEscenario();
		assertTrue("si existe el veritice", grafo.contains("a"));
	}
	public void testAdjuntos()
	{
		setupEscenario();
		assertEquals("el numero de vertices adj deberia ser 2", 2,grafo.getVertex("a").getAdj().size() );
		setupEscenario();
		Bag<Edge<Vertex>> vertices = grafo.getVertex("b").getAdj();
		String verticesm="";
		for (Edge<Vertex> edge : vertices) {
			verticesm+= edge.getDestino().getNombre();
		}
		assertEquals("el mensaje deberia ser ", "eda",verticesm);
		
	}
	public void testMst()
	{
		setupEscenarioMst();
		EdgeWeightedGraph prueba = mst.graphMst();
		assertEquals("el peso total deberia ser 5.5", 5.5, prueba.getPeso());
		
	}
}
