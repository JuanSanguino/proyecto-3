package model.data_structures;

import java.util.ArrayList;

import junit.framework.TestCase;

public class RedBlackBSTTests extends TestCase 
{
	private RedBlackBST<Integer,String> redBlackTree;
	
	private void setupEscenario0()
	{
		redBlackTree = new RedBlackBST<Integer,String>();
	}
	private void setupEscenario1()
	{
		redBlackTree = new RedBlackBST<Integer,String>();
		redBlackTree.put(1, "a");
	}
	private void setupEscenario2()
	{
		redBlackTree = new RedBlackBST<Integer,String>();
		redBlackTree.put(1, "a");
		redBlackTree.put(2, "b");
		redBlackTree.put(3, "c");
		redBlackTree.put(4, "d");
		redBlackTree.put(5, "e");
	}
	public void testGet()
	{
		setupEscenario0();
		assertNull("No deber�a retornar ning�n objeto", redBlackTree.get(1));
		
		setupEscenario1();
		assertEquals("El elemento retornado no concuerda con el buscado", "a", redBlackTree.get(1));
		
		
		setupEscenario2();
		assertEquals("", "c", redBlackTree.get(3));
	}
	public void testSize()
	{
		setupEscenario0();
		assertEquals("No deber�a haber ning�n elemento", 0, redBlackTree.size());
		
		setupEscenario1();
		assertEquals("Deber�a haber un elemento", 1, redBlackTree.size());
		
		setupEscenario2();
		assertEquals("Deber�a haber m�s de un elemento", 5, redBlackTree.size());
	}
	
	public void testContains()
	{
		setupEscenario0();
		assertFalse("El �rbol deber�a estar vac�o", redBlackTree.contains(5));
		
		setupEscenario1();
		assertTrue("El �rbol deber�a contener la llave", redBlackTree.contains(1));
		assertFalse("El �rbol no deber�a contener la llave", redBlackTree.contains(2));
		
		
		setupEscenario2();
		assertTrue("El �rbol deber�a contener la llave", redBlackTree.contains(1));
		assertTrue("El �rbol deber�a contener la llave", redBlackTree.contains(2));
		assertTrue("El �rbol deber�a contener la llave", redBlackTree.contains(3));
	}
	
	public void testHeight()
	{
		setupEscenario0();
		assertEquals("El �rbol no deber�a tener ning�n elemento",-1, redBlackTree.height());
		
		setupEscenario1();
		assertEquals("El �rbol no deber�a tener altura", 0, redBlackTree.height());
		
		setupEscenario2();
		assertEquals("El �rbol no tiene la altura correspondida", 2, redBlackTree.height());
	}
	
	public void testMin()
	{
		setupEscenario0();
		assertNull("El �rbol deber�a estar vac�o", redBlackTree.min());
			
		setupEscenario2();
		assertEquals("El m�nimo no concuerda", new Integer(1), redBlackTree.min());
	}
	
	public void testMax()
	{
		setupEscenario0();
		assertNull("El �rbol deber�a estar vac�o", redBlackTree.max());
		
		setupEscenario1();
		assertEquals("El m�ximo no concuerda", new Integer(1), redBlackTree.max());
		
		setupEscenario2();
		assertEquals("El m�ximo no cuerda", new Integer(5), redBlackTree.max());
	}
	
	public void testInOrden()
	{
		setupEscenario0();
		assertEquals("No deber�a haber un recorrido en in orden", null, redBlackTree.inOrden());
		
		setupEscenario1();
		assertEquals("Deber�a haber al menos un elemento en el recorrido", 1, redBlackTree.inOrden().size());
		
		setupEscenario2();
		assertEquals("Deber�a haber m�s de un elemento en el recorrido", 5, redBlackTree.inOrden().size());
		
		setupEscenario2();
		ArrayList<String> inorden = redBlackTree.inOrden();
		assertEquals("Deber�a ser b", "c", inorden.get(2));
		
	}
}
